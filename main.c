#include <stdio.h>
#include <stdlib.h>

#define YEAR 6
#define GRASS  100
#define RABBIT 3
#define TIGER  1
#define TIGER_LIFE 3

typedef struct {
	unsigned int grass;
	unsigned int rabbit;		// Rabbit eats grass.
	unsigned int *tigers;
	unsigned int tiger_amount;
} World;

World createWorld();
void showWorld(World *world);
void newYear(World *world);

int main () 
{	
	World world = createWorld();
	
	showWorld(&world);

	for(short y=0; y < YEAR; y++) {
		newYear(&world);
		showWorld(&world);
	}

	return 0;
}

World createWorld()
{
	World result;

	result.grass  = GRASS;
	result.rabbit = RABBIT;
	result.tiger_amount = TIGER;
	result.tigers = (unsigned int *) malloc (sizeof(unsigned int) * result.tiger_amount);
	for(int i=0; i < TIGER; i++) {
		result.tigers[i] = TIGER_LIFE;
	}
	return result;
}

void showWorld(World *world)
{
	printf("Grass:  %d\n", world->grass);
	printf("Rabbit: %d\n", world->rabbit);
	printf("Tiger:  %d\n", world->tiger_amount);
	for(unsigned int i=0; i < world->tiger_amount; i++) {
		printf("%d  ",world->tigers[i]);
	}
	printf("\n-------------------\n");
}

void newYear(World *world)
{
	world->grass -= world->rabbit;
	world->grass *= 2;

	world->rabbit *= 2;
	world->rabbit -= world->tiger_amount;
	
	// each tiger is getting old.
	unsigned int mount = world->tiger_amount;
	for(unsigned int i=0; i<mount; i++) {
		if(i!=mount-1) world->tigers[i]--;

		// if empty, tiger die.
		if(!world->tigers[i]) world->tiger_amount--;
	}


	world->tiger_amount++;
	world->tigers = (unsigned int *) realloc (world->tigers ,sizeof(unsigned int) * world->tiger_amount);
	mount = world->tiger_amount;
	printf("mount=%d\n", mount);
	for(unsigned int j=0; j<world->tiger_amount; j++) {
		// printf("world->tigers=%d\n",world->tigers[i]);
		world->tigers[j] = TIGER_LIFE;
		printf("world->tigers[%d]=%d\n",j,TIGER_LIFE);
		break;
		
	}
	
}